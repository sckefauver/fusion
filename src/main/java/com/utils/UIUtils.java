package com.utils;

import com.BreedpixTables;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

/**
 * 
 * @author Alexi Akl
 *
 */
@SuppressWarnings("restriction")
public class UIUtils {  
  public static TextArea logArea;
  public static TextField sheetId;
  public static TextField sheetName;
  public static TextField fromRow;
  public static TextField toRow;
  public static TextField columns;
  public static ImageView imageView;
  public static Text imageTitle;
  public static Button buttonSave;
  public static Button buttonSettings;
  public static Button buttonStart;
  public static Button buttonStop;
  public static CheckBox processAllCheckbox;
  public static CheckBox processContinuouslyCheckbox;
  public static CheckBox processUnprocessedCheckbox;
  public static TextField columnNameFilterKey;
  public static TextField columnNameFilterValue;
  public static DatePicker fromDatePicker;
  public static DatePicker toDatePicker;
  public static Stage settingsStage;
  static VBox filtervbox;
  private static StringConverter<LocalDate> dateConverter;
  
  public static Scene buildUI() {
    dateConverter = new StringConverter<LocalDate>() {
      private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

      @Override
      public String toString(LocalDate localDate) {
        if(localDate == null) {
          return "";
        }
        return dateTimeFormatter.format(localDate);
      }
      
      @Override
      public LocalDate fromString(String dateString) {
        if(dateString == null || dateString.trim().isEmpty()) {
          return null;
        }
        return LocalDate.parse(dateString, dateTimeFormatter);
      }
    };
    
    createSettingsStage();
    
    BorderPane root = new BorderPane();
    HBox hbox = addHBox();
    root.setTop(hbox);
    VBox vbox = addVBox();
    root.setLeft(vbox);
    VBox imagevbox = addImageVBox();
    root.setCenter(imagevbox);

    return new Scene(root, 820, 600);
  }
  
  public static void appendLog(final String string) {
  Calendar cal = Calendar.getInstance();
  SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
      final String append = sdf.format(cal.getTime()) + " " + string + "\n";
      if (Platform.isFxApplicationThread()) {
          logArea.setText(append + logArea.getText());
      } else {
          Platform.runLater(new Runnable() {
              @Override
              public void run() {
                  logArea.setText(append + logArea.getText());
              }
          });
      }
  }

  public static VBox addImageVBox() {
    VBox vbox = new VBox();
    vbox.setPadding(new Insets(10));
    vbox.setSpacing(8);

    imageTitle = new Text("Images");
    imageTitle.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    vbox.getChildren().add(imageTitle);

    imageView = new ImageView();
    imageView.setFitHeight(350);
    imageView.setFitWidth(350);
    imageView.setPreserveRatio(true);
    
    vbox.getChildren().add(imageView);

    return vbox;
  }

  private static Node getProcessAllCheckbox() {
      processAllCheckbox = new CheckBox("Process all records in table");
      processAllCheckbox.setFont(Font.font("Arial", FontWeight.BOLD, 14));
      processAllCheckbox.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent arg0) {
              filtervbox.setVisible(!processAllCheckbox.isSelected());
              processContinuouslyCheckbox.setSelected(false);
              processUnprocessedCheckbox.setDisable(false);
          }
      });
      return processAllCheckbox;
  }

  private static Node getContinuouslyProcessCheckbox() {
      processContinuouslyCheckbox = new CheckBox("Continuously process records");
      processContinuouslyCheckbox.setFont(Font.font("Arial", FontWeight.BOLD, 14));
      processContinuouslyCheckbox.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent arg0) {
              if (processContinuouslyCheckbox.isSelected()) {
                  processAllCheckbox.setSelected(false);
                  filtervbox.setVisible(true);
                  processUnprocessedCheckbox.setSelected(true);
                  processUnprocessedCheckbox.setDisable(true);
              } else {
                  processUnprocessedCheckbox.setDisable(false);
              }
          }
      });
      return processContinuouslyCheckbox;
  }

  public static VBox addVBox() {
    VBox vbox = new VBox();
    vbox.setPadding(new Insets(10));
    vbox.setSpacing(8);

    Text title = new Text("Logs");
    title.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    vbox.getChildren().add(title);

    logArea = new TextArea();
    logArea.setPrefWidth(400);
    logArea.setEditable(false);
    logArea.setFont(Font.font("Arial", FontWeight.EXTRA_LIGHT, 10));
    vbox.getChildren().add(logArea);

    Text columnNameFilterLabel = new Text("Filter by");
    columnNameFilterLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));

    filtervbox = new VBox();
    filtervbox.setPadding(new Insets(10));
    filtervbox.setSpacing(8);
    filtervbox.getChildren().add(columnNameFilterLabel);

    processUnprocessedCheckbox = new CheckBox("Only unprocessed images");
    processUnprocessedCheckbox.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    processUnprocessedCheckbox.setSelected(true);
    
    HBox columnhbox = new HBox();
    columnhbox.setPadding(new Insets(0, 0, 0, 0));
    columnhbox.setSpacing(5);
    
    columnNameFilterKey = new TextField("");
    columnNameFilterKey.setPromptText("Column Name");
    columnNameFilterKey.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    
    columnNameFilterValue = new TextField("");
    columnNameFilterValue.setPromptText("Column Value");
    columnNameFilterValue.setFont(Font.font("Arial", FontWeight.BOLD, 14));

    columnhbox.getChildren().add(columnNameFilterKey);
    columnhbox.getChildren().add(columnNameFilterValue);
    
    HBox datehbox = new HBox();
    datehbox.setPadding(new Insets(0, 0, 0, 0));
    datehbox.setSpacing(5);

    fromDatePicker = new DatePicker();
    fromDatePicker.setConverter(dateConverter);
    toDatePicker = new DatePicker();
    toDatePicker.setConverter(dateConverter);
    datehbox.getChildren().add(fromDatePicker);
    datehbox.getChildren().add(toDatePicker);

    filtervbox.getChildren().add(processUnprocessedCheckbox);
    filtervbox.getChildren().add(columnhbox);
    filtervbox.getChildren().add(datehbox);

    vbox.getChildren().add(getProcessAllCheckbox());
    vbox.getChildren().add(getContinuouslyProcessCheckbox());
    vbox.getChildren().add(filtervbox);
    return vbox;
  }

  public static HBox addHBox() {
    HBox hbox = new HBox();
    hbox.setPadding(new Insets(15, 12, 15, 12));
    hbox.setSpacing(10);
    hbox.setStyle("-fx-background-color: #336699;");

    buttonSettings = new Button("Settings");
    buttonSettings.setPrefSize(100, 20);
    buttonSettings.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        openSettings(event);
      }
    });
    
    buttonStart = new Button("Start");
    buttonStart.setPrefSize(100, 20);

    buttonStop = new Button("Stop");
    buttonStop.setPrefSize(100, 20);
    buttonStop.setDisable(true);

    Button buttonClear = new Button("Clear logs");
    buttonClear.setPrefSize(100, 20);
    buttonClear.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        logArea.setText("");
      }
    });
    
    Button buttonRefreshToken = new Button("Re-Auth");
    buttonRefreshToken.setPrefSize(100, 20);
    buttonRefreshToken.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        String[]entries = BreedpixTables.TOKENS_DIRECTORY_PATH.list();
        for(String s: entries){
            File currentFile = new File(BreedpixTables.TOKENS_DIRECTORY_PATH.getPath(),s);
            currentFile.delete();
        }
        BreedpixTables.initFusionAPI();
      }
    });
    hbox.getChildren().addAll(buttonSettings, buttonStart, buttonStop, buttonClear, buttonRefreshToken);

    return hbox;
  }
  
  static void createSettingsStage() {
    settingsStage = new Stage();
    
    VBox vbox = new VBox();
    vbox.setPadding(new Insets(10));
    vbox.setSpacing(8);
    
    Text sheetIdLabel = new Text("Sheet ID");
    sheetIdLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    vbox.getChildren().add(sheetIdLabel);
    
    sheetId = new TextField(BreedpixTables.sheetId);
    sheetId.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    vbox.getChildren().add(sheetId);

    Text sheetNameLabel = new Text("Sheet Name");
    sheetNameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    vbox.getChildren().add(sheetNameLabel);

    sheetName = new TextField(BreedpixTables.sheetName);
    sheetName.setFont(Font.font("Arial", FontWeight.BOLD, 14));

    Text fromRowToRowLabel = new Text("From Row : To Row");
    fromRowToRowLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    
    fromRow = new TextField(BreedpixTables.fromRow);
    fromRow.setPromptText("From Row");
    fromRow.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    
    Text semiColon = new Text(" : ");
    semiColon.setFont(Font.font("Arial", FontWeight.BOLD, 14));
    
    toRow = new TextField(BreedpixTables.toRow);
    toRow.setPromptText("To Row");
    toRow.setFont(Font.font("Arial", FontWeight.BOLD, 14));

    HBox cellshbox = new HBox();
    cellshbox.setPadding(new Insets(0, 0, 0, 0));
    cellshbox.setSpacing(5);
    cellshbox.getChildren().add(fromRow);
    cellshbox.getChildren().add(semiColon);
    cellshbox.getChildren().add(toRow);

    sheetName = new TextField(BreedpixTables.sheetName);
    sheetName.setFont(Font.font("Arial", FontWeight.BOLD, 14));

    vbox.getChildren().add(sheetName);
    vbox.getChildren().add(fromRowToRowLabel);
    vbox.getChildren().add(cellshbox);
    
    buttonSave = new Button("Save");
    buttonSave.setPrefSize(100, 20);
    vbox.getChildren().add(buttonSave);
    
    Button buttonClose = new Button("Close");
    buttonClose.setPrefSize(100, 20);
    vbox.getChildren().add(buttonClose);
    buttonClose.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        settingsStage.hide();
      }
    });
    
    Scene settingsScene = new Scene(vbox, 500, 400);
    
    settingsStage.setScene(settingsScene);
    settingsStage.setTitle("Settings");
    settingsStage.initModality(Modality.WINDOW_MODAL);
  }
  
  static void openSettings(ActionEvent event) {
    sheetId.setText(BreedpixTables.sheetId);
    sheetName.setText(BreedpixTables.sheetName);
    fromRow.setText(BreedpixTables.fromRow);
    toRow.setText(BreedpixTables.toRow);
    if (settingsStage.getOwner() == null) {
      settingsStage.initOwner(((Node) event.getSource()).getScene().getWindow());
    }
    settingsStage.show();
  }
}
