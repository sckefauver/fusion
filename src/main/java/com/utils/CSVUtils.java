package com.utils;

import com.breedpix.BreedPixResult;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Alexi Akl
 *
 */
public class CSVUtils {

  public static File csvFile;

  public static boolean createCSVFile() {
    String delim = ",";
    StringBuilder csvBuilder = new StringBuilder();
    csvBuilder.append("Record ID");
    csvBuilder.append(delim);
    csvBuilder.append("Intensity");
    csvBuilder.append(delim);
    csvBuilder.append("Hue");
    csvBuilder.append(delim);
    csvBuilder.append("Saturation");
    csvBuilder.append(delim);
    csvBuilder.append("Lightness");
    csvBuilder.append(delim);
    csvBuilder.append("a*");
    csvBuilder.append(delim);
    csvBuilder.append("b*");
    csvBuilder.append(delim);
    csvBuilder.append("u*");
    csvBuilder.append(delim);
    csvBuilder.append("v*");
    csvBuilder.append(delim);
    csvBuilder.append("GA");
    csvBuilder.append(delim);
    csvBuilder.append("GGA");
    csvBuilder.append(delim);
    csvBuilder.append("CSI");
    csvBuilder.append(System.getProperty("line.separator"));
    
    String csvFilename = new SimpleDateFormat("yyyyMMddHHmmss'.csv'").format(new Date());
    File csvDirectory = new File("csv");
    csvFile = new File(csvDirectory.getAbsolutePath()+File.separator+csvFilename);
    try {
      if (!csvDirectory.exists()) {
        if(!csvDirectory.mkdir()) {
          return false;
        }
      }
      if (!csvFile.createNewFile()) {
        return false;
      }
      Files.write(Paths.get(csvFile.getAbsolutePath()), csvBuilder.toString().getBytes(), StandardOpenOption.APPEND);
      UIUtils.appendLog("CSV file created: " + csvFile.getAbsolutePath());
    } catch (IOException e) {
      UIUtils.appendLog("Failed to create CSV file, aborting");
      return false;
    }
    
    return true;
  }

  public static void appendResultToCSV(BreedPixResult result, String recordId) {
    String delim = ",";
    StringBuilder csvBuilder = new StringBuilder();

    csvBuilder.append(recordId);
    csvBuilder.append(delim);
    csvBuilder.append(result.getIhs_i());
    csvBuilder.append(delim);
    csvBuilder.append(result.getIhs_h());
    csvBuilder.append(delim);
    csvBuilder.append(result.getIhs_s());
    csvBuilder.append(delim);
    csvBuilder.append(result.getLab_l());
    csvBuilder.append(delim);
    csvBuilder.append(result.getLab_a());
    csvBuilder.append(delim);
    csvBuilder.append(result.getLab_b());
    csvBuilder.append(delim);
    csvBuilder.append(result.getLuv_u());
    csvBuilder.append(delim);
    csvBuilder.append(result.getLuv_v());
    csvBuilder.append(delim);
    csvBuilder.append(result.getGa());
    csvBuilder.append(delim);
    csvBuilder.append(result.getGga());
    csvBuilder.append(delim);
    csvBuilder.append(result.getCsi());
    csvBuilder.append(System.getProperty("line.separator"));
    
    try {
      Files.write(Paths.get(csvFile.getAbsolutePath()), csvBuilder.toString().getBytes(), StandardOpenOption.APPEND);
      UIUtils.appendLog("Result appended to CSV file");
    } catch (IOException e) {
      UIUtils.appendLog("Failed to append result to CSV file");
    }
  }
}
