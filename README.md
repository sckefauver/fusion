# Breedpix Tables
Quick description of the Application:
1. Set Sheet ID, Sheet name and Rows range in the Settings pane
2. Authorize application by clicking Re-Auth
3. Process records by clicking Start
4. Results will be automatically saved to Google Sheets and to CSV files

## Start Application
You can start the application by going inside `data` folder and launching application `breedpix-fusion.jar` or by running in the command line: `java -jar breedpix-fusion.jar`

Optional: You can use Maven to run the application by going to the root directory and running: `mvn exec:java`

To install Maven: http://www.baeldung.com/install-maven-on-windows-linux-mac

## Filtering
1. Process all records available in the Google Sheet by checking `Process all records in table`
2. Filter records to process by unchecking `Process all records in table` and picking your filter options:
	* Select `Only unprocessed images` to skip all previously processed images
	* Set `Column key` and `column value` to filter by column value, eg: `treatment = road`
	* Set `From date` and `To Date` to filter by `metasubmissiondate` date column

## CSV
CSV export contains the following data:
1. Record ID
2. Intensity
3. Hue
4. Saturation
5. Lightness
6. a*
7. b*
8. u*
9. v*
10. GA
11. GGA
12. CSI

![CSV](data/images/csv.png "CSV")

## Sheets API
To connect to the Sheets API you need client id and secret which are saved in `src/main/resources/client_secrets.json`

If you wish to use new credentials you need to:
1. create OAuth 2.0 client ID here: https://console.cloud.google.com/apis/credentials?project=universitatbarcelonadoctorxlai
2. Update the `client_secrets.json` file
3. Optional: Create a new runnable JAR file if you choose to run it using a JAR

# Authors

- Dr. Shawn Kefauver
   - Project Principal Investigator, University of Barcelona
- [Jose Armando Fernandez Gallego](https://integrativecropecophysiology.com/academic-staff/phd-students/fernandez-gallego-jose-armando/)
   - Algorithm Development, University of Barcelona
- Alexi Akl
   - Software Engineer, [Postlight](https://postlight.com)
- George El-Haddad
   - Software Engineer, [Postlight](https://postlight.com)

# License

Copyright 2018 Shawn Carlisle Kefauver

Licensed under the General Public License version 3.0

- [http://www.gnu.org/licenses/gpl-3.0.en.html](http://www.gnu.org/licenses/gpl-3.0.en.html)
- [https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3))